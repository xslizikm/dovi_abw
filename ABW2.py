#!/usr/bin/python
#TODO napojit hosta na switch pri vypisoch
import requests as rq
import json
import time
import sys
from collections import namedtuple,defaultdict
from heapq import heappush, heappop


#function for dijkstra algorithm
def Dijkstra(graph, start):
    A = {}
    for i in graph:
     A[i] = None
    queue = [(0, start)]
    while queue:
        path_len, v = heappop(queue)
        if A[v] is None: # v is unvisited
            A[v] = path_len
            for w, edge_len in graph[v].items():
                if A[w] is None:
                    if path_len == 0:
                     heappush(queue, ( edge_len, w))
                    elif path_len > edge_len:
                     heappush(queue, ( path_len, w))
                    else:
                     heappush(queue, ( edge_len, w))
    for i in A:
     if A[i] !=  None:
      print "DIJKSTRA Z  " + start +" DO "+ i + " MAX ABW :  " + str(float((A[i] * -1)/1000000)) + ' Mbit/s'
     else:
      print "DIJKSTRA Z  " + start +" DO "+ i + " MAX ABW :  " + str(float(A[i]/1000000)) + ' Mbit/s'
    print "\n"
    return [0 if x is None else x for x in A]

## MENU
if len(sys.argv) <= 2:
 print "Pre spustenie programu je potrebne zadat dva argumenty: prvy argument je ako casto sa ma dopytovat counter na switchy v sekundach"
 print "Druhy argument urcuje v akom mode sa program spusti, argumentom \"f\" sa program spusti v mode v ktorom pocita Availible Bandwidth pre fixed paths"
 print "Argument \"d\" spusti program v mode v ktorom pocita maximalny Availible bandwidth pre kazdy vrchol do kazdeho vrcholu"
 print "Priklad spustenia programu je ./ABW.py 2 d"
 print "Program je mozno ukoncit cez klavesovu skratku CTRL-C"
 sys.exit()
if int(sys.argv[1]) <= 0 or (sys.argv[2] != 'd' and sys.argv[2] != 'f'):
 print "Zadali ste nespravne argumenty, pre zobrazenie helpu spustite program bez argumentov"
 sys.exit()

#ipadress of controller host
address="localhost"
SwitchToSwitchABW = namedtuple("SwitchToSwitchABW",["src_switch","dst_switch","availableBW"])
Link = namedtuple("Link",["src_switch","dst_switch"])
MyStruct = namedtuple("MyStruct",["linkBW","src_switch","dst_switch"])
MyStruct2 = namedtuple("MyStruct2",["linkBW","src_switch","dst_switch","src_port","dst_port"])
MyStruct3 = namedtuple("MyStruct3",["linkBW","src_switch","dst_switch","src_port","dst_port","availableBW"])
MyStruct4 = namedtuple("MyStruct4",["sender","reciever"])
LinkArray = []

# Change Routing policy in floodlight
dt = { 'metric':'utilization' }
p = rq.post('http://localhost:8080/wm/routing/metric/json',json = dt)
print(p.text)
#Generate info about links in topology
links = rq.get('http://localhost:8080/wm/topology/links/json')
print(links.content)
#enable statistics in floodlight
st = rq.post('http://localhost:8080/wm/statistics/config/enable/json')
print(st.text)

time.sleep(10)

# sem je potrebne vpisat vsetky kapacity linkov urcene v mininete
link1 = MyStruct(linkBW=10,src_switch="00:00:00:00:00:00:00:01",dst_switch="00:00:00:00:00:00:00:02")
link2 = MyStruct(linkBW=20,src_switch="00:00:00:00:00:00:00:02",dst_switch="00:00:00:00:00:00:00:03")
link3 = MyStruct(linkBW=10,src_switch="00:00:00:00:00:00:00:03",dst_switch="00:00:00:00:00:00:00:04")
link4 = MyStruct(linkBW=10,src_switch="00:00:00:00:00:00:00:02",dst_switch="00:00:00:00:00:00:00:04")
link5 = MyStruct(linkBW=5,src_switch="00:00:00:00:00:00:00:01",dst_switch="00:00:00:00:00:00:00:04")
#assignment of link capacity to switches:

for i in json.loads(links.content):
    if i['src-switch'] == link1[1] and i['dst-switch'] == link1[2]:
        LinkArray.append(MyStruct2(linkBW=link1[0],src_switch=i['src-switch'],dst_switch=i['dst-switch'],src_port=i['src-port'],dst_port=i['dst-port']))
    elif i['src-switch'] == link2[1] and i['dst-switch'] == link2[2]:
        LinkArray.append(MyStruct2(linkBW=link2[0],src_switch=i['src-switch'],dst_switch=i['dst-switch'],src_port=i['src-port'],dst_port=i['dst-port']))
    elif i['src-switch'] == link3[1] and i['dst-switch'] == link3[2]:
        LinkArray.append(MyStruct2(linkBW=link3[0],src_switch=i['src-switch'],dst_switch=i['dst-switch'],src_port=i['src-port'],dst_port=i['dst-port']))
    elif i['src-switch'] == link4[1] and i['dst-switch'] == link4[2]:
        LinkArray.append(MyStruct2(linkBW=link4[0],src_switch=i['src-switch'],dst_switch=i['dst-switch'],src_port=i['src-port'],dst_port=i['dst-port']))
    elif i['src-switch'] == link5[1] and i['dst-switch'] == link5[2]:
        LinkArray.append(MyStruct2(linkBW=link5[0],src_switch=i['src-switch'],dst_switch=i['dst-switch'],src_port=i['src-port'],dst_port=i['dst-port']))

#calculate available bandwith for every link DONE

try:
 while True:
## calculate avalilible bandwith for every link DONE
   FinalArray = []
   for item in LinkArray:
           bwRq = rq.get('http://'+address+':8080/wm/statistics/bandwidth/' + item.dst_switch + '/' + str(item.dst_port) + '/json')
           bwRs = json.loads(bwRq.content)
           dstBWrx = bwRs[0]['bits-per-second-rx']
           dstBWtx = bwRs[0]['bits-per-second-tx']
           timestamp = bwRs[0]['updated']          
           print ' Namerane hodnoty RX a TX bits-per-second na switchi : '+item.dst_switch +' porte: '+ str(item.dst_port) + '  rx:  ' + str(dstBWrx) + '  tx: ' + str(dstBWtx) + '  timestamp: '+ timestamp
           if dstBWrx == '' :
               dstBWrx = 0
           if dstBWtx == '' :
               dstBWtx = 0
           dstBW = int(dstBWrx)#  + int(dstBWtx))
           FinalArray.append(MyStruct3(linkBW=item.linkBW,src_switch=item.src_switch,dst_switch=item.dst_switch,src_port=item.src_port,dst_port=item.dst_port,availableBW=(item.linkBW*1000000)-dstBW))
           print ' Available BW na linku : ' + item.src_switch + 'dst  ' + item.dst_switch + '  SRC PORT : ' + str(item.src_port) + ' DST PORT : ' + str(item.dst_port) + ' =  '   + str((float(item.linkBW*1000000-dstBW))/1000000) + 'Mbit/s'
           print '\n'
   if (sys.argv[2]) == 'd':
#DIJKSTRA
    vertices = []
    for i in FinalArray:
     vertices.append(i.src_switch)
     vertices.append(i.dst_switch)
    vertices = list(set(vertices))
    AdjacentVertices = defaultdict(dict)
    for i in vertices:
     for j in FinalArray:
      if i == j.src_switch:
       AdjacentVertices[i][j.dst_switch] = j.availableBW * -1
      elif i == j.dst_switch:
       AdjacentVertices[i][j.src_switch] = j.availableBW * -1
    #print AdjacentVertices
    for i in AdjacentVertices:
     Dijkstra(AdjacentVertices,i)
#check attachement points of all hosts DONE
   elif sys.argv[2] == 'f': 
    deviceRq = rq.get('http://'+address+':8080/wm/device/all/json')
    deviceRs = json.loads(deviceRq.content)
    DeviceArray = []
    HostStruct = namedtuple("HostStruct",["mac","switch","port"])
    for key in deviceRs:
     for key1 in deviceRs[key]:
      mac = key1["mac"][0]
      for key2 in key1["attachmentPoint"]:
        DeviceArray.append(HostStruct(mac=mac,switch=key2["switch"],port=key2["port"]))
##check all active flows besides controller generated ones -- and return distinct flows in network -DONE
    flowRq = rq.get('http://'+address+':8080/wm/core/switch/all/flow/json')
    flowRs = json.loads(flowRq.content)
    flows = []
    for key in flowRs:
     for key1 in flowRs[key]:
      for key2 in flowRs[key][key1]:
           if 'eth_src' in key2["match"]:
               tuple = (key2["match"]["eth_src"],key2["match"]["eth_dst"])
               flows.append(tuple)
    distinctFlowsList = list(set(flows))
    FlowsArray = []
    for key in distinctFlowsList:
     sender = key[0]
     reciever = key[1]
     FlowsArray.append(MyStruct4(sender=key[0],reciever=key[1]))
## map hosts generating flows onto attachment point and get route betweeen hosts DONE
     SwitchAbwArray = []
     for keya in FlowsArray:
      for key in DeviceArray:
       if keya[0] == key[0]:
         src_dpid = key[1]
         src_port = key[2]
       elif keya[1] == key[0]:
         dst_dpid = key[1]
         dst_port = key[2]
      SwitchAbwArray.append(SwitchToSwitchABW(src_switch=src_dpid,dst_switch=dst_dpid,availableBW=0))
#Create path from links
      routeRq = rq.get('http://'+address+':8080/wm/routing/path/'+src_dpid+'/'+src_port+'/'+dst_dpid+'/'+dst_port+'/json')
      routeRs = json.loads(routeRq.content) 
      for key in routeRs:
        b = 0
        SwitchArray = []
        for k in routeRs[key][1:][:-1]:
         b = b+1
         if "switch" in k and b % 2 == 1:
          tmp = k["switch"]
         elif "switch" in k:
          SwitchArray.append(Link(src_switch=tmp,dst_switch=k["switch"]))
 #calculate availible bandwidth
        minBW = 99999999999
        PathArray = []
        PathArray.append(sender)
        for i in SwitchArray:
         for j in FinalArray:
          if ((i.src_switch == j.src_switch and i.dst_switch == j.dst_switch) and j.availableBW < minBW) or ((i.src_switch == j.dst_switch and i.dst_switch == j.src_switch) and j.availableBW < minBW):
           minBW = j.availableBW
         if len(PathArray) == 1:
           PathArray.append(i.src_switch)
         PathArray.append(i.dst_switch)
     PathArray.append(reciever)
     resultString = 'cesta: '
#Get path + abw
     for l in PathArray:
       resultString = resultString + ' : ' + l
     print (resultString) 
     print 'dostupny bandwidth pre cestu : '  + str(float(minBW)/1000000)+' Mbit/s'
   time.sleep(1)
except KeyboardInterrupt:
 print 'END'
